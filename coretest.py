import sys
import audiogen
from itertools import zip_longest


class AudioGenTest:
    """
    The primary test object
    """

    def __init__(self):
        self.unit = audiogen.util.constant(1)

    def _setup_mux(self):
        return audiogen.util.mixer(
            (iter(list(range(10))),), ((self.unit,), (self.unit,))
        )

    def test_mux_output_channels(self):
        output = self._setup_mux()

        assert len(output) == 2

    def test_mux_duplicated_content(self):
        output = self._setup_mux()
        saved = [list(channel) for channel in output]

        # NOTE Debug messages
        for num, channel in enumerate(saved):
            print(
                (
                    "DEBUG `test_mux_duplicated_content()`: Channel {}: {}".format(
                        num, ", ".join([str(c) for c in channel])
                    )
                )
            )

        for channel in saved:
            for sample, test in zip_longest(channel, list(range(10))):
                assert sample == test


"""
Run testing
"""
print("Starting test..")
test_obj = AudioGenTest()  # test obj init
test_obj.test_mux_duplicated_content()  # test for dupe content
test_obj.test_mux_output_channels()  # test output channels
print("Test ran sucsessfully!")
